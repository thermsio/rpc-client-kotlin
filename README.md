## RPC-Client-Kotlin

![rpc_logo.png](https://bitbucket.org/thermsio/rpc-server-ts/raw/a9f154178cbc69e9b821ed22fbe7bfec125cdf86/rpc_logo.png)

An easy-to-use client library specifically developed for Android platform to interact with the server RPC framework.

- Node.js RPC server (https://bitbucket.org/thermsio/rpc-server-ts)
- JS/TS client (https://bitbucket.org/thermsio/rpc-client-ts)
- iOS/Swift client (https://bitbucket.org/thermsio/rpc-client-swift)


Importing Library 
--------

You can use gradle to import library to your app.
#
[![](https://jitpack.io/v/org.bitbucket.thermsio/rpc-client-kotlin.svg)](https://jitpack.io/#org.bitbucket.thermsio/rpc-client-kotlin)

```
allprojects {
    repositories {
        maven { url 'https://jitpack.io' }
    }
}
```

```
dependencies {
    implementation 'org.bitbucket.thermsio:rpc-client-kotlin:master-SNAPSHOT'
}
```


Samples
-------
Check out the Example App on this [link](https://bitbucket.org/thermsio/rpc-client-kotlin-example/) for library features demonstration & their relevent usage.
##