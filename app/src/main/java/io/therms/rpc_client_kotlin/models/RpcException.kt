package io.therms.rpc_client_kotlin.models

import java.lang.Exception

class RpcException(message: String?, var parent: Exception? = null) : Exception(message) {
}